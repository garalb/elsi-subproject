#
#
# -- Search for ELPA using pkg-config data
# (This, with further refinemens,
#  should go in a module in the cmake subdir)
# 
# There are several non-standard issues:
#
# 1. The pkgconfig filename might be of the form
#
#     elpa-2020.05.001.rc1.pc
#
# instead of simply 'elpa.pc'.
#
# 2. The include directory entry in the pkgconfig file
#    is missing the 'modules' leaf component.
#
# The most expedient solution for (1) is to put a symbolic
# link to the desired version somewhere in the PKG_CONFIG_PATH. For example:
#
#   ln -sf /path/to/original/.pc/file $HOME/lib/pkgconfig/elpa.pc
#
#   export PKG_CONFIG_PATH=$HOME/lib/pkgconfig:${PKG_CONFIG_PATH}
#
# (Note: CMAKE_PREFIX_PATH can also be used, but without the
#  '/lib/pkgconfig' part.
#
#  Another solution is to use pkg_search_module, as below, but
#  one has to enumerate all possibilities.
#
# A solution for (2) is encoded below.
# 
find_package(PkgConfig REQUIRED QUIET)

message(CHECK_START "Searching for ELPA library")
if( SIESTA_WITH_OPENMP )
  pkg_check_modules(ELPA elpa_openmp)
  if( NOT ELPA_FOUND )
    message(WARNING "ELPA could not locate elpa_openmp package, resorting to non-threaded version")
    pkg_check_modules(ELPA elpa)
  endif()
else()
  pkg_search_module(ELPA elpa elpa-2020.05.001 elpa-2021.05.001 elpa-2021.11.001)
endif()

if(NOT ELPA_FOUND)
  message(CHECK_FAIL "not found")
  return()
endif()

message(DEBUG "ELPA_LIBDIR: ${ELPA_LIBDIR}")
message(DEBUG "ELPA_LIBRARIES: ${ELPA_LIBRARIES}")
message(DEBUG "ELPA_LINK_LIBRARIES: ${ELPA_LINK_LIBRARIES}")
message(DEBUG "ELPA_INCLUDEDIR: ${ELPA_INCLUDEDIR}")
message(DEBUG "ELPA_INCLUDE_DIRS: ${ELPA_INCLUDE_DIRS}")

# Retrieve compilation flags
# Note the use of the retrieved module name from the call to pkg_search_module
pkg_get_variable(ELPA_FCFLAGS ${ELPA_MODULE_NAME} fcflags)
message(DEBUG "ELPA_FCFLAGS: ${ELPA_FCFLAGS}")

#
# Fix non-standard setting of Fortran module directory
#
if(ELPA_FCFLAGS)
  message(STATUS "Fixing include directories from FCFLAGS")
  list(FILTER ELPA_FCFLAGS INCLUDE REGEX "^-I" )
  string(REPLACE "-I" "" ELPA_Fortran_INCLUDE_DIRS ${ELPA_FCFLAGS})
else()
  message(STATUS "Adding include-directories manually")
  # TODO this should probably be a loop since there may be several include dir's
  set(ELPA_Fortran_INCLUDE_DIRS "${ELPA_INCLUDE_DIRS}/modules")
endif()

message(STATUS "ELPA Fortran search path to be used: ${ELPA_Fortran_INCLUDE_DIRS}")

message(CHECK_PASS "found")

# Manually adding the library
add_library(Elpa::elpa INTERFACE IMPORTED)
target_link_libraries(Elpa::elpa INTERFACE ${ELPA_LINK_LIBRARIES})
target_include_directories(Elpa::elpa INTERFACE ${ELPA_Fortran_INCLUDE_DIRS})

#
# Check ELPA options, GPU kernel names, etc
#

if (
    (DEFINED ELPA_SET_GPU_STRING) AND
    (DEFINED ELPA_SET_REAL_GPU_KERNEL) AND
    (DEFINED ELPA_SET_COMPLEX_GPU_KERNEL)
    )

  # Assume that the user knows the information

  set(ELPA_GPU_STRING ${ELPA_SET_GPU_STRING})
  set(ELPA_REAL_GPU_KERNEL ${ELPA_REAL_GPU_KERNEL})
  set(ELPA_COMPLEX_GPU_KERNEL ${ELPA_COMPLEX_GPU_KERNEL})

  message(STATUS "-- GPU kernels and GPU setting string for ELPA set externally to:")
  message(STATUS "     ELPA_GPU_STRING: ${ELPA_GPU_STRING}")
  message(STATUS "     ELPA_REAL_GPU_KERNEL ${ELPA_REAL_GPU_KERNEL}")
  message(STATUS "     ELPA_COMPLEX_GPU_KERNEL ${ELPA_COMPLEX_GPU_KERNEL}")
  message(STATUS "-- without checking the actual library capabilities")
  
  set(ELPA_HAS_GPU_SUPPORT TRUE)  # We must trust the user

elseif(NOT ELPA_AVOID_PRINT_KERNELS)

  # Try to get the information from executing the 'elpa2_print_kernels' program

  find_program(
    ELPA_PRINT_KERNELS elpa2_print_kernels
    PATHS "${ELPA_PREFIX}/bin")
  
  if(NOT ELPA_PRINT_KERNELS)
    message(WARNING "elpa2_print_kernels not found. GPU kernel info will need to be entered manually")
  endif()
  message(DEBUG "ELPA_PRINT_KERNELS: ${ELPA_PRINT_KERNELS}")

  # Check for GPU support
  execute_process(
    COMMAND ${ELPA_PRINT_KERNELS}
    COMMAND grep GPU
    RESULT_VARIABLE _RESULT)

  if(_RESULT EQUAL "0")
    # There is compiled GPU support
    set(ELPA_HAS_GPU_SUPPORT TRUE)
    
    # We now search for the names of the GPU kernels
    
    foreach(_kind REAL COMPLEX)

      message(DEBUG "Looking for ${_kind} GPU kernel name...")

      execute_process(
	COMMAND ${ELPA_PRINT_KERNELS}
        COMMAND grep GPU
        COMMAND grep ${_kind}
        OUTPUT_VARIABLE _var
        RESULT_VARIABLE _RESULT)
      
      if(NOT _RESULT EQUAL "0")
        message(WARNING "Cannot find ${_kind} GPU kernel name. Set ELPA_SET_${_kind}_GPU_KERNEL manually")
      else()
        string(STRIP ${_var} _var_strip)     # Remove leading blanks
	#message(STATUS "_var_strip: ${_var_strip}")
        string(REPLACE " " ";" _var_list ${_var_strip})  # Make a list
	#message(STATUS "_var_list: ${_var_list}")
        list(POP_FRONT _var_list ELPA_${_kind}_GPU_KERNEL) # Get first element
        message(STATUS "ELPA_${_kind}_GPU_KERNEL: ${ELPA_${_kind}_GPU_KERNEL}")
      endif()

    endforeach()

    string(FIND ${ELPA_REAL_GPU_KERNEL} "_NVIDIA_" _is_nvidia)
    string(FIND ${ELPA_REAL_GPU_KERNEL} "_AMD_" _is_amd)
    if(NOT _is_nvidia EQUAL -1)
      set(ELPA_GPU_STRING "nvidia-gpu")
    elseif(NOT _is_amd  EQUAL -1)
      set(ELPA_GPU_STRING "amd-gpu")
    else()
      set(ELPA_GPU_STRING "unknown-gpu")
      message(WARNING "You need to set ELPA_SET_GPU_STRING manually")
    endif()
    message(STATUS "ELPA_GPU_STRING: ${ELPA_GPU_STRING}")

  endif() 

else()
  # Default values if we do not find information

  set(ELPA_HAS_GPU_SUPPORT FALSE)
  set(ELPA_GPU_STRING "")
  set(ELPA_REAL_GPU_KERNEL -1)
  set(ELPA_COMPLEX_GPU_KERNEL -1)


endif()

message(STATUS "ELPA_HAS_GPU_SUPPORT: ${ELPA_HAS_GPU_SUPPORT}")
