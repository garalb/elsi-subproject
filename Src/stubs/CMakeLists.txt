# This is probably the simplest solution
# to add the C++ runtime to the linking statement
# Useful for, e.g., PEXSI
#
add_library(cxx_dummy_lib dummy.cpp)

target_include_directories(cxx_dummy_lib
  INTERFACE
  "${CMAKE_CURRENT_BINARY_DIR}"
)
